package main

import (
	"github.com/jinzhu/gorm"
	pb "gitlab.com/ajimenez2712-shippy/user-service/proto/auth"
)

type Repository interface {
	Create(user *pb.User) error
	GetAll() ([]*pb.User, error)
	Get(id string) (*pb.User, error)
	GetByEmailAndPassword(user *pb.User) (*pb.User, error)
}

func (repo *UserRepository) Create(user *pb.User) error {
	if err := repo.db.Create(user).Error; err != nil {
		return err
	}
	return nil
}

type UserRepository struct {
	db *gorm.DB
}

func (repo *UserRepository) GetAll() ([]*pb.User, error) {
	var users []*pb.User
	if err := repo.db.Find(&users).Error; err != nil {
		return nil, err
	}
	return users, nil
}

func (repo *UserRepository) Get(id string) (*pb.User, error) {
	var user *pb.User
	user.Id = id
	if err := repo.db.First(&user).Error; err != nil {
		return nil, err
	}
	return user, nil
}

func (repo *UserRepository) GetByEmailAndPassword(user *pb.User) (*pb.User, error) {
	if err := repo.db.First(&user).Error; err != nil {
		return nil, err
	}
	return user, nil
}
