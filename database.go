package main

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

const (
	maxRetries = 5
)

func retry(attempts int, sleep time.Duration, callback func() (*gorm.DB, error)) (db *gorm.DB, err error) {
	for i := 0; ; i++ {
		db, err = callback()
		if err == nil {
			return
		}

		if i >= (attempts - 1) {
			break
		}

		time.Sleep(sleep)

		log.Println("retrying after error:", err)
	}
	return nil, fmt.Errorf("after %d attempts, last error: %s", attempts, err)
}

func CreateConnection() (*gorm.DB, error) {
	dbName := os.Getenv("DB_NAME")
	host := os.Getenv("DB_HOST")
	port := os.Getenv("DB_PORT")
	user := os.Getenv("DB_USER")
	password := os.Getenv("DB_PASSWORD")

	return retry(5, 2*time.Second, func() (*gorm.DB, error) {
		return gorm.Open(
			"postgres",
			fmt.Sprintf(
				"host=%s port=%s user=%s dbname=%s sslmode=disable password=%s",
				host, port, user, dbName, password,
			),
		)
	})
}
